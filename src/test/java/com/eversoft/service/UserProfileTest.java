package com.eversoft.service;

import com.eversoft.config.security.WebSecurityConfig;
import com.eversoft.dto.ProfileDto;
import com.eversoft.model.Profile;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.RoleRepository;
import com.eversoft.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserProfileTest {

    @Mock
    private UserRepository userRepository;
    private UserService userService;
    private RoleTypeService roleTypeService;
    private RoleService roleService;
    private RoleRepository roleRepository;
    private ProfileRepository profileRepository;
    private TokenService tokenService;
    private WebSecurityConfig webSecurityConfig;
    private ProfileService profileService;
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        ModelMapper modelMapper = new ModelMapper();
        userService = new UserServiceImpl(userRepository, roleTypeService, roleRepository,
                tokenService, modelMapper, passwordEncoder);
        profileService = new ProfileServiceImpl(userRepository, modelMapper);

    }

    @Test
    public void testGetUserProfile() {
        final String id = "xyz";
        final String email = "admin@wp.pl";
        final String fb = "fejs";
        final String linkdin = "link";

        when(userRepository.findById(id))
                .thenReturn(Optional.of(User.builder()
                        .id(id)
                        .profile((Profile.builder().linkedinId(linkdin).fbId(fb).build()))
                        .email(email)
                        .build()));

        ProfileDto profileDto = profileService.getProfileByUserId(id);

        Assert.assertEquals(fb, profileDto.getFbId());
        Assert.assertEquals(linkdin, profileDto.getLinkedinId());
    }

}
