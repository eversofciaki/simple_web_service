package com.eversoft.service;

import com.eversoft.config.security.WebSecurityConfig;
import com.eversoft.dto.UserDto;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.RoleRepository;
import com.eversoft.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    private UserService userService;
    private RoleTypeService roleTypeService;
    private RoleRepository roleRepository;
    private TokenService tokenService;
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        ModelMapper modelMapper = new ModelMapper();
        userService = new UserServiceImpl(userRepository, roleTypeService, roleRepository,
                tokenService, modelMapper, passwordEncoder);
    }

    @Test
    public void testFindUserByEmail() {
        final String id = "id-id";
        final String email = "admin@wp.pl";

        when(userRepository.findByEmail(email))
                .thenReturn(Optional.of(User.builder()
                        .id(id)
                        .email(email)
                        .build()));

        UserDto userDto = userService.getUserByEmail(email);

        Assert.assertEquals(email, userDto.getEmail());
        Assert.assertEquals(id, userDto.getId());
    }

    @Test
    public void testFindUserById() {
        final String id = "id-id";
        final String email = "admin@wp.pl";

        when(userRepository.findById(id))
                .thenReturn(Optional.of(User.builder()
                        .id(id)
                        .email(email)
                        .build()));

        UserDto userDto = userService.getUserById(id);

        Assert.assertEquals(email, userDto.getEmail());
        Assert.assertEquals(id, userDto.getId());
    }


}
