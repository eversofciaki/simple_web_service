package com.eversoft.password;

import com.eversoft.dto.UserRegistrationDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class PasswordTest {


    private static Validator validator;

    @Test
    public void testInvalidPassword() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        UserRegistrationDto userRegistration = new UserRegistrationDto();
        userRegistration.setName("memory");
        userRegistration.setSurname("not found");
        userRegistration.setEmail("info@memorynotfound.com");
        userRegistration.setPassword("password");
        userRegistration.setConfirmPassword("password");

        Set<ConstraintViolation<UserRegistrationDto>> constraintViolations = validator.validate(userRegistration);

        Assert.assertEquals(constraintViolations.size(), 2);
    }

    @Test
    public void testValidPasswords() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        UserRegistrationDto userRegistration = new UserRegistrationDto();
        userRegistration.setName("memory");
        userRegistration.setSurname("not found");
        userRegistration.setEmail("info@memorynotfound.com");
        userRegistration.setPassword("xJ3!dij50");
        userRegistration.setConfirmPassword("xJ3!dij50yyyy");

        Set<ConstraintViolation<UserRegistrationDto>> constraintViolations = validator.validate(userRegistration);

        Assert.assertEquals(constraintViolations.size(), 0);
    }


}
