package com.eversoft.web;

import com.eversoft.SimpleApplication;
import com.eversoft.SimpleApplicationTest;
import com.eversoft.dto.ProfileEditDto;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleApplicationTest.class)
@TestPropertySource(locations = "classpath:application.yml")
@ActiveProfiles("test")
@ContextConfiguration(classes = {SimpleApplication.class})
public class ProfileControllerTest {

    @Autowired
    private WebApplicationContext context;

    protected MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenCorrectEmail_whenFindUserByEmail_expectOj() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/api/users/profile/7b3898b4-98f7-11ea-bb37-0242ac130002")).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void editProfileByUserId_checkResponse_expectOk() throws Exception {
        String fb = "fejs2";
        String linkdin = "link2";

        ProfileEditDto profileEditDto = ProfileEditDto.builder().fbId(fb).linkedinId(linkdin).build();
        Gson gson = new Gson();
        String json = gson.toJson(profileEditDto);

        ResultActions resultActions = mockMvc.perform(
                post("/api/users/profile/editByUserId=8b844984-98f7-11ea-bb37-0242ac130002")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk()).andDo(print())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.fbId").value(fb))
                .andExpect(jsonPath("$.linkedinId").value(linkdin));
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void deleteUserProfile_By_UserId() throws Exception {

        ResultActions resultActions = mockMvc.perform(
                delete("/api/users/profile/delete?userId=7b3898b4-98f7-11ea-bb37-0242ac130002"))
                .andExpect(status().isOk()).andDo(print())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.fbId").doesNotExist())   //expected: NULL
                .andExpect(jsonPath("$.linkedinId").doesNotExist());    //expected: NULL
    }

}
