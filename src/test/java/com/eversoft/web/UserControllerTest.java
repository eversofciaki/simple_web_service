package com.eversoft.web;

import com.eversoft.SimpleApplication;
import com.eversoft.SimpleApplicationTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleApplicationTest.class)
@TestPropertySource(locations = "classpath:application.yml")
@ActiveProfiles("test")
@ContextConfiguration(classes = {SimpleApplication.class})
public class UserControllerTest {
    @Autowired
    private WebApplicationContext context;

    protected MockMvc mockMvc;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenIncorrectEmail_whenFindUserByEmail_expectNotFoundException() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/api/users/findUser/admin@wp.plXYZ")
        ).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenCorrectEmail_whenFindUserByEmail_expectOk() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/api/users/findUser/admin@wp.pl")
        ).andExpect(status().isOk());

        Assert.assertTrue(resultActions.andReturn().getResponse().getContentAsString().contains("admin@wp.pl"));
    }


}
