package com.eversoft.exception;

public class BadRequestException extends BaseException {

    public BadRequestException(ErrorCode errorCode) {
        super(errorCode);
    }

    public BadRequestException() {
        super(ErrorCode.BAD_REQUEST);
    }

}
