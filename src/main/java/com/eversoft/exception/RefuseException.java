package com.eversoft.exception;

public class RefuseException extends UnauthorizedException {

    public RefuseException() {
        super(ErrorCode.FORBIDDEN);
    }

}
