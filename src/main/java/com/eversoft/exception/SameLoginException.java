package com.eversoft.exception;

public class SameLoginException extends BadRequestException {

    public SameLoginException() {
        super(ErrorCode.LOGIN_ALREADY_IN_USE);

    }
}
