package com.eversoft.exception;

public class BadPasswordException extends NotFoundException {

    public BadPasswordException() {
        super(ErrorCode.UNAUTHORIZED);
    }

}
