package com.eversoft.exception;

public class UserDeletedException extends UnauthorizedException {

    public UserDeletedException() {
        super(ErrorCode.USER_DELETED);
    }

}
