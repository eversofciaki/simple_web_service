package com.eversoft.exception;

public enum ErrorCode {
    NOT_FOUND(404),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    BAD_REQUEST(400),
    LOGIN_ALREADY_IN_USE(40001),
    CAN_NOT_FIND_A_TOKEN(40099),
    USER_DELETED(40098),
    FIELD_NOT_FOUND(40501);


    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
