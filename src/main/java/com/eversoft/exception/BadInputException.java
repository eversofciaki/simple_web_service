package com.eversoft.exception;

public class BadInputException extends BadRequestException {

    public BadInputException() {
        super(ErrorCode.FIELD_NOT_FOUND);
    }

}
