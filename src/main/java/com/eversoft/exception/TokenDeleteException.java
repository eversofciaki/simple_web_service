package com.eversoft.exception;

public class TokenDeleteException extends UnauthorizedException {

    public TokenDeleteException() {
        super(ErrorCode.CAN_NOT_FIND_A_TOKEN);
    }

}
