package com.eversoft.web;

import com.eversoft.dto.*;
import com.eversoft.service.ProfileService;
import com.eversoft.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@Log4j
@RequestMapping(value = "/api/users")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {
    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<UserDto> registerUser(@RequestBody @Valid UserRegistrationDto userRegistrationDto) {
        return ResponseEntity.ok(userService.saveUser(userRegistrationDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping("/findUser/{email}")
    public ResponseEntity<UserDto> findUser(@PathVariable String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping("/getUser/{userId}")
    public ResponseEntity<UserDto> findUserById(@PathVariable String userId) {
        return ResponseEntity.ok(userService.getUserById(userId));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<Page<UserDto>> listAllUsers(@RequestBody UsersQueryDto usersQueryDto, Pageable pageable) {
        return ResponseEntity.ok(userService.getAll(usersQueryDto, pageable));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @DeleteMapping("/delete/{userId}")
    public ResponseEntity<UserDto> deleteUserById(@PathVariable String userId) {
        return ResponseEntity.ok(userService.deleteUserById(userId));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @DeleteMapping("/deleteByEmail")
    public ResponseEntity<UserDto> deleteUserByEmail(@RequestParam String email) {
        return ResponseEntity.ok(userService.deleteUserByEmail(email));
    }

    @PreAuthorize("hasAuthority('EDIT_USER_ADMIN')")
    @PostMapping("/edit")
    public ResponseEntity<UserDto> editUserByUserId(@RequestParam String userId, @RequestBody @Valid UserEditDto userEditDto) {
        return ResponseEntity.ok(userService.editUserByUserId(userId, userEditDto));
    }

}
