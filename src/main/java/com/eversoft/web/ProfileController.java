package com.eversoft.web;

import com.eversoft.dto.ProfileDto;
import com.eversoft.dto.ProfileEditDto;
import com.eversoft.service.ProfileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Log4j
@RequestMapping(value = "/api/users/profile")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfileController {
    private final ProfileService profileService;


    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/edit")
    public ResponseEntity<ProfileDto> editProfile(@RequestBody @Valid ProfileEditDto profileEditDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return ResponseEntity.ok(profileService.editProfile(auth.getName(), profileEditDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN')")
    @PostMapping("/editByEmail={email}")
    public ResponseEntity<ProfileDto> editUserProfile(@PathVariable String email,
                                                      @RequestBody @Valid ProfileEditDto profileEditDto) {
        return ResponseEntity.ok(profileService.editUserProfile(email, profileEditDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @PostMapping("/editByUserId={userId}")
    public ResponseEntity<ProfileDto> editUserProfileByUserId(@PathVariable String userId,
                                                              @RequestBody @Valid ProfileEditDto profileEditDto) {
        return ResponseEntity.ok(profileService.editUserProfileByUserId(userId, profileEditDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping("/{userId}")
    public ResponseEntity<ProfileDto> getProfileByUserId(@PathVariable String userId) {
        return ResponseEntity.ok(profileService.getProfileByUserId(userId));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @DeleteMapping("/delete")
    public ResponseEntity<ProfileDto> deleteProfileByUserId(@RequestParam String userId) {
        return ResponseEntity.ok(profileService.deleteUserProfileByUserId(userId));
    }

}
