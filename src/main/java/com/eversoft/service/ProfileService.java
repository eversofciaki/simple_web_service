package com.eversoft.service;

import com.eversoft.dto.ProfileDto;
import com.eversoft.dto.ProfileEditDto;

public interface ProfileService {

    ProfileDto getProfileByUserId(String userId);

    ProfileDto editProfile(String authEmail, ProfileEditDto profileEditDto);

    ProfileDto editUserProfile(String email, ProfileEditDto profileEditDto);

    ProfileDto editUserProfileByUserId(String userId, ProfileEditDto profileEditDto);

    ProfileDto deleteUserProfileByUserId(String userId);
}
