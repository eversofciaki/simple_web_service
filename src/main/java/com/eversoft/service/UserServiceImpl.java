package com.eversoft.service;


import com.eversoft.config.security.WebSecurityConfig;
import com.eversoft.dto.*;
import com.eversoft.exception.*;
import com.eversoft.model.*;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.RoleRepository;
import com.eversoft.repository.UserRepository;
import com.querydsl.core.BooleanBuilder;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.apache.commons.lang3.StringUtils;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleTypeService roleTypeService;
    private final RoleRepository roleRepository;
    private final TokenService tokenService;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDto getUserByEmail(String email) {
        return mapUserToUserDto(userRepository.findByEmail(email).orElseThrow(NotFoundException::new));
    }

    @Override
    public UserDto saveUser(UserRegistrationDto userRegistrationDto) {

        if (userRepository.existsByEmail(userRegistrationDto.getEmail()))
            throw new SameLoginException();

        if (!userRegistrationDto.getPassword().equals(userRegistrationDto.getConfirmPassword()))
            throw new BadPasswordException();

        User user = modelMapper.map(userRegistrationDto, User.class);

        Profile profile = new Profile();
        user.setProfile(profile);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        RoleType rolaType = roleTypeService.getRoleByName(RoleTypeEnum.USER);

        role.setRoleType(rolaType);
        role.setUser(user);
        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);

        return mapUserToUserDto(user);
    }


    @Override
    public UserDto getUserById(String id) {
        return mapUserToUserDto(userRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    @Override
    public UserDto deleteUserById(String id) {
        User user = userRepository.findById(id).orElseThrow(NotFoundException::new);
        user.setDeleted(true);
        tokenService.deleteToken(user.getEmail());
        userRepository.save(user);
        return mapUserToUserDto(user);
    }

    @Override
    public UserDto deleteUserByEmail(String email) {
        User user = modelMapper.map(
                userRepository.findByEmail(email).orElseThrow(NotFoundException::new), User.class);
        user.setDeleted(true);
        tokenService.deleteToken(user.getEmail());
        userRepository.save(user);
        return mapUserToUserDto(user);
    }


    @Override
    public Page<UserDto> getAll(UsersQueryDto dto, Pageable pageable) {
        BooleanBuilder where = new BooleanBuilder();
        if (StringUtils.isNotBlank(dto.getEmail()))
            where.and(QUser.user.email.containsIgnoreCase(dto.getEmail()));
        if (StringUtils.isNotBlank(dto.getName()))
            where.and(QUser.user.name.containsIgnoreCase(dto.getName()));
        if (StringUtils.isNotBlank(dto.getName()))
            where.and(QUser.user.surname.containsIgnoreCase(dto.getSurname()));
        if (Boolean.TRUE.equals(dto.getDeleted()))
            where.and(QUser.user.deleted.isTrue());
        if (Boolean.FALSE.equals(dto.getDeleted()))
            where.and(QUser.user.deleted.isFalse());

        if (dto.getProfile() != null) {
            if (StringUtils.isNotBlank(dto.getProfile().getFbId()))
                where.and(QUser.user.profile.fbId.containsIgnoreCase(dto.getProfile().getFbId()));
            if (StringUtils.isNotBlank(dto.getProfile().getLinkedinId()))
                where.and(QUser.user.profile.linkedinId.containsIgnoreCase(dto.getProfile().getLinkedinId()));
        }

        Page<User> page = userRepository.findAll(where.getValue(), pageable);

        return page.map(x -> mapUserToUserDto(x));
    }


    @Override
    public UserDto editUserByUserId(String userId, UserEditDto userEditDto) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());

        User user = userRepository.findById(userId).orElseThrow(NotFoundException::new);

        Set<Role> roles = new HashSet<>();

        if (userEditDto.getRoles() == null || userEditDto.getRoles().isEmpty()) {
        } else {
            tokenService.deleteToken(user.getEmail());
            for (Role roleDel : user.getRoles()) {
                roleDel.setDeleted(true);
                roleRepository.save(roleDel);
            }
            user.setRoles(null);
            for (int roleEnum : userEditDto.getRoles()) {
                Role role = new Role();
                RoleType roleType = roleTypeService.getRoleByName(RoleTypeEnum.of(roleEnum));
                role.setRoleType(roleType);
                role.setUser(user);
                roles.add(role);
            }
            user.setRoles(roles);
        }

        modelMapper.map(userEditDto, user);
        user.setRoles(roles);
        userRepository.save(user);

        return mapUserToUserDto(user);
    }

    @Override
    public UserDto mapUserToUserDto(User user) {
        UserDto userDto = new UserDto();
        modelMapper.map(user, userDto);

        if (user.getProfile() != null) {
            ProfileDto profileDto = new ProfileDto();

            modelMapper.map(user.getProfile(), profileDto);
            userDto.setProfileDto(profileDto);
        }

        if (user.getRoles() == null || user.getRoles().isEmpty()) {
            return userDto;
        } else {
            Set<RoleDto> roleOut = new HashSet<>();
            for (Role x : user.getRoles()) {
                RoleDto roleDto = new RoleDto();
                RoleTypeDto roleTypeDto = new RoleTypeDto();
                modelMapper.map(x.getRoleType(), roleTypeDto);
                modelMapper.map(x, roleDto);
                roleDto.setRoleTypeDto(roleTypeDto);
                roleOut.add(roleDto);
            }
            return userDto;
        }
    }

}
