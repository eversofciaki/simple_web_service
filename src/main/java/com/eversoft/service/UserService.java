package com.eversoft.service;

import com.eversoft.dto.*;
import com.eversoft.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    UserDto getUserByEmail(String email);

    UserDto saveUser(UserRegistrationDto userRegistrationDto);

    UserDto getUserById(String userId);

    UserDto deleteUserById(String userId);

    UserDto deleteUserByEmail(String email);

    Page<UserDto> getAll(UsersQueryDto usersQueryDto, Pageable pageable);

    UserDto editUserByUserId(String userId, UserEditDto userEditDto);

    UserDto mapUserToUserDto(User user);

}
