package com.eversoft.service;

import com.eversoft.dto.RoleDto;

import java.util.List;

public interface RoleService {

    RoleDto addRole(RoleDto roleDto);
}
