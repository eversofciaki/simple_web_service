package com.eversoft.service;

import com.eversoft.dto.RoleTypeDto;
import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;


public interface RoleTypeService {

    RoleType getRoleByName(RoleTypeEnum name);

}
