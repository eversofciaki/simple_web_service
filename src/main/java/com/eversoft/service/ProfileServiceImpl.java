package com.eversoft.service;

import com.eversoft.dto.ProfileDto;
import com.eversoft.dto.ProfileEditDto;
import com.eversoft.dto.UserDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.Profile;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public ProfileDto getProfileByUserId(String userId) {
        User user = userRepository.findById(userId).orElseThrow(NotFoundException::new);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto editProfile(String authEmail, ProfileEditDto profileEditDto) {
        User user = userRepository.findByEmail(authEmail).orElseThrow(NotFoundException::new);
        user.getProfile().setLinkedinId(profileEditDto.getLinkedinId());
        user.getProfile().setFbId(profileEditDto.getFbId());
        userRepository.save(user);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto editUserProfile(String email, ProfileEditDto profileEditDto) {
        User user = userRepository.findByEmail(email).orElseThrow(NotFoundException::new);
        user.getProfile().setLinkedinId(profileEditDto.getLinkedinId());
        user.getProfile().setFbId(profileEditDto.getFbId());
        userRepository.save(user);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto editUserProfileByUserId(String userId, ProfileEditDto profileEditDto) {
        User user = userRepository.findById(userId).orElseThrow(NotFoundException::new);
        user.getProfile().setLinkedinId(profileEditDto.getLinkedinId());
        user.getProfile().setFbId(profileEditDto.getFbId());
        userRepository.save(user);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto deleteUserProfileByUserId(String userId) {
        User user = userRepository.findById(userId).orElseThrow(NotFoundException::new);
        user.getProfile().setDeleted(true);
        userRepository.save(user);
        Profile profile = new Profile();
        user.setProfile(profile);
        userRepository.save(user);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

}
