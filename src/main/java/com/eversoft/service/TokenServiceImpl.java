package com.eversoft.service;

import com.eversoft.config.security.AuthorizationServerConfig;
import com.eversoft.exception.TokenDeleteException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.provider.approval.Approval;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenServiceImpl implements TokenService {

    private final AuthorizationServerConfig authorizationServerConfig;

    @Value("${oauth2.clientId}")
    private String clientId;

    @Override
    public void deleteToken(String username) {
        try {
            Collection<Approval> tokens = authorizationServerConfig.approvalStore(authorizationServerConfig.tokenStore()).getApprovals(username, clientId);
            authorizationServerConfig.approvalStore(authorizationServerConfig.tokenStore()).revokeApprovals(tokens);
        } catch (TokenDeleteException e) {
            e.printStackTrace();
        }
    }
}
