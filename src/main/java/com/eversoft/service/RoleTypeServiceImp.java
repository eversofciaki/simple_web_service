package com.eversoft.service;

import com.eversoft.dto.RoleTypeDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;
import com.eversoft.repository.RoleTypeRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleTypeServiceImp implements RoleTypeService {

    private final RoleTypeRepository roleTypeRepository;
    private final ModelMapper modelMapper;

    @Override
    public RoleType getRoleByName(RoleTypeEnum name) {
        return roleTypeRepository.findByName(name).orElseThrow(NotFoundException::new);
    }

}
