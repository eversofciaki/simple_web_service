package com.eversoft.model;

import java.util.stream.Stream;

public enum RoleTypeEnum {
    USER(0),
    EDIT_PROFILE_ADMIN(1),
    EDIT_USER_ADMIN(2);

    private int value;

    RoleTypeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RoleTypeEnum of(int value) {
        return Stream.of(RoleTypeEnum.values())
                .filter(p -> p.getValue() == value)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
