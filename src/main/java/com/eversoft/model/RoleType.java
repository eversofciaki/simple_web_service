package com.eversoft.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public class RoleType extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(unique = true, insertable = false, updatable = false, nullable = false)
    private RoleTypeEnum name;

}
