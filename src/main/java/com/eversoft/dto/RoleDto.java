package com.eversoft.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoleDto {

    @NotNull
    private String userId;

    @NotNull
    private RoleTypeDto roleTypeDto;

    public RoleDto() {
    }

    public RoleDto(@NotNull String userId, @NotNull RoleTypeDto roleTypeDto) {
        this.userId = userId;
        this.roleTypeDto = roleTypeDto;
    }

}
