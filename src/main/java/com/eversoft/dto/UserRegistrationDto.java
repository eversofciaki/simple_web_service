package com.eversoft.dto;

import com.eversoft.config.validation.ValidPassword;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class UserRegistrationDto {

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    @NotBlank
    @ValidPassword
    private String password;

    @NotBlank
    @ValidPassword
    private String confirmPassword;

    @Email
    @NotBlank
    private String email;

}
