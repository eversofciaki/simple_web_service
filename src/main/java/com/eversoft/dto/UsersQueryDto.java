package com.eversoft.dto;

import lombok.Data;

@Data
public class UsersQueryDto {

    private String email;

    private String name;

    private String surname;

    private Boolean deleted;

    private ProfileDto profile;

}
