package com.eversoft.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProfileEditDto {

    private String fbId;

    private String linkedinId;

}
