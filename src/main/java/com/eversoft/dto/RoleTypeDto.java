package com.eversoft.dto;

import com.eversoft.model.RoleTypeEnum;
import lombok.Data;

@Data
public class RoleTypeDto {

    private String id;

    private RoleTypeEnum name;

}
