package com.eversoft.dto;

import com.eversoft.model.Profile;
import lombok.Data;

import java.util.Set;

@Data
public class UserEditDto {

    private String name;

    private String surname;

    private Boolean deleted;

    private ProfileDto profile;

    private Set<Integer> roles;

}
