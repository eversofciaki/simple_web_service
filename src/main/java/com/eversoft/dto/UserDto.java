package com.eversoft.dto;

import com.eversoft.model.Profile;
import com.eversoft.model.Role;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
public class UserDto {
    private String id;

    private String email;

    private String name;

    private String surname;

    private Boolean deleted;

    private ProfileDto profileDto;

    private Set<RoleDto> roles;

}
