package com.eversoft.config.security;

import com.eversoft.exception.ErrorCode;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exc) throws IOException, ServletException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            log.warn(String.format("User: %s attempted to access the protected URL: %s", auth.getName(), request.getRequestURI()));
        }
        response.setStatus(ErrorCode.FORBIDDEN.getCode());
        response.getWriter().append(ErrorCode.FORBIDDEN.name());
    }

}
