package com.eversoft.config.security;


import com.eversoft.exception.NotFoundException;
import com.eversoft.exception.UserDeletedException;
import com.eversoft.model.Role;
import com.eversoft.model.User;
import com.eversoft.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Primary
@Log4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username).orElseThrow(NotFoundException::new);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        if (!Boolean.FALSE.equals(user.getDeleted())) {
            throw new UserDeletedException();
        } else {
            for (Role role : user.getRoles()) {
                if (Boolean.FALSE.equals(role.getDeleted())) {
                    authorities.add(new SimpleGrantedAuthority(role.getRoleType().getName().toString()));
                }
            }
            return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
        }
    }


}
