package com.eversoft.config;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.MDC;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Log4j
public class LoggingAspect {
    @Before("execution(* com.eversoft.web.*.*(..))")
    public void logControllerCalls(JoinPoint joinPoint) {
        CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();

        StringBuilder params = new StringBuilder();
        for (int i = 0; i < joinPoint.getArgs().length; i++) {
            params.append(codeSignature.getParameterNames()[i])
                    .append(" = ")
                    .append(joinPoint.getArgs()[i].toString())
                    .append(" ");
        }
        String className = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        MDC.put("userId", userName);
        log.info(String.format("%s with arguments %s called in %s by %s", methodName, params.toString(), className, userName));
    }
}
