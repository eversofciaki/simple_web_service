package com.eversoft.config;

import com.eversoft.dto.ErrorResponseDto;
import com.eversoft.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.bind.annotation.ExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({BadRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDto badRequest(BaseException e) {
        return new ErrorResponseDto(e.getErrorCode().getCode());
    }

    @ExceptionHandler({UnauthorizedException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponseDto unauthorized(BaseException e) {
        return new ErrorResponseDto(e.getErrorCode().getCode());
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponseDto notFound(BaseException e) {
        return new ErrorResponseDto(e.getErrorCode().getCode());
    }

}
