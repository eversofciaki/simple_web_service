-- --
-- --ver2
-- INSERT INTO ROLE_TYPE VALUES ('4f1651cc-98f7-11ea-bb37-0242ac130002', 'USER');
-- INSERT INTO ROLE_TYPE VALUES ('5ef64a20-98f7-11ea-bb37-0242ac130002', 'EDIT_PROFILE_ADMIN');
-- INSERT INTO ROLE_TYPE VALUES ('697c3c16-98f7-11ea-bb37-0242ac130002', 'EDIT_USER_ADMIN');

---
INSERT INTO ROLE_TYPE VALUES ('4f1651cc-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'USER');
INSERT INTO ROLE_TYPE VALUES ('5ef64a20-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'EDIT_PROFILE_ADMIN');
INSERT INTO ROLE_TYPE VALUES ('697c3c16-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'EDIT_USER_ADMIN');

--
--ver2
INSERT INTO PROFILE VALUES ('3ce759d8-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'fb_id_1', 'linkedin_id_1');
INSERT INTO PROFILE VALUES ('a56798f6-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'fb_id_2', 'linkedin_id_2');
INSERT INTO PROFILE VALUES ('bb66cabe-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'fb_id_3', 'linkedin_id_3');
INSERT INTO PROFILE VALUES ('c69cb5c4-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'fb_id_3', 'linkedin_id_3');
--
--ver2
INSERT INTO users_table VALUES ('d16d7912-98f6-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'admin@wp.pl', 'sylwia','$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam',  'nowak', '3ce759d8-98f7-11ea-bb37-0242ac130002');
INSERT INTO users_table VALUES ('7b3898b4-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'piotr', 'piotr', '$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam',  'kowal', 'a56798f6-98f7-11ea-bb37-0242ac130002');
INSERT INTO users_table VALUES ('8b844984-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'janusz@wp.pl', 'janusz', '$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam',  'zeta', 'bb66cabe-98f7-11ea-bb37-0242ac130002');
INSERT INTO users_table VALUES ('962a8d62-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, 'iza@wp.pl', 'iza', '$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam', 'gama', 'c69cb5c4-98f7-11ea-bb37-0242ac130002');
--
-- SYWLIA ALL ROLES; PIOTR USER; ver2
INSERT INTO ROLE VALUES ('d083bf9c-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, '4f1651cc-98f7-11ea-bb37-0242ac130002', 'd16d7912-98f6-11ea-bb37-0242ac130002');
INSERT INTO ROLE VALUES ('d598ce96-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, '5ef64a20-98f7-11ea-bb37-0242ac130002', 'd16d7912-98f6-11ea-bb37-0242ac130002');
INSERT INTO ROLE VALUES ('dab7902e-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, '697c3c16-98f7-11ea-bb37-0242ac130002', 'd16d7912-98f6-11ea-bb37-0242ac130002');
INSERT INTO ROLE VALUES ('dbd69536-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, '4f1651cc-98f7-11ea-bb37-0242ac130002', '7b3898b4-98f7-11ea-bb37-0242ac130002');
INSERT INTO ROLE VALUES ('e5234102-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, '5ef64a20-98f7-11ea-bb37-0242ac130002', '8b844984-98f7-11ea-bb37-0242ac130002');
INSERT INTO ROLE VALUES ('ec0f0d34-98f7-11ea-bb37-0242ac130002','auto-generate', '2020-01-01', null, null, 0,false, '697c3c16-98f7-11ea-bb37-0242ac130002', '962a8d62-98f7-11ea-bb37-0242ac130002');


