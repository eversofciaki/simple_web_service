create table profile (id varchar(255) not null, created_by varchar(255), created_date timestamp, last_modified_by varchar(255), last_modified_date timestamp, version bigint, deleted boolean, fb_id varchar(255), linkedin_id varchar(255), primary key (id));
create table role (id varchar(255) not null, created_by varchar(255), created_date timestamp, last_modified_by varchar(255), last_modified_date timestamp, version bigint, deleted boolean, role_type_id varchar(255) not null, user_id varchar(255), primary key (id));
create table role_type (id varchar(255) not null, created_by varchar(255), created_date timestamp, last_modified_by varchar(255), last_modified_date timestamp, version bigint, deleted boolean, name varchar(255) not null, primary key (id));
create table users_table (id varchar(255) not null, created_by varchar(255), created_date timestamp, last_modified_by varchar(255), last_modified_date timestamp, version bigint, deleted boolean, email varchar(255) not null, name varchar(255), password varchar(255) not null, surname varchar(255), profile_id varchar(255) not null, primary key (id));
alter table role_type add constraint UK_ouutblmtrjnx7hn1ahri6swhj unique (name);
alter table users_table add constraint UK_k9gbvc9xvagimmj1y060txmha unique (email);
alter table role add constraint FKawrres8h1p9kky45mmwqhj86o foreign key (role_type_id) references role_type;
alter table role add constraint FKgfb001ty0d5f12v06ymfolfrc foreign key (user_id) references users_table;
alter table users_table add constraint FK39uoltopjhccxiutyns1qvgol foreign key (profile_id) references profile;
